package com.example.servicediscoveryapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class ServiceDiscoveryApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceDiscoveryApiApplication.class, args);
	}

}
