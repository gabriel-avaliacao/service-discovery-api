**Service Discovery Api**

#### Netflix Eureka Service
###### Service Discovery

---

#### Rabbitmq 
Quando subir o Service Discovery lembre-se de rodar o **docker-compose up -d** na raiz do projeto
para subir o **Rabbitmq**


#### Para acessar o Rabbit basta acessar o endereço: http://localhost:15672/

---

## Este serviço em conjunto com mais 2 contempla o projeto como um todo 

#### 1 - Fanatical Partner API - https://bitbucket.org/gabriel-avaliacao/fanatical-partner-api.git
#### 2 - Campaign API - https://bitbucket.org/gabriel-avaliacao/campaign-api.git
#### 3 - Service Discovery API - https://bitbucket.org/gabriel-avaliacao/service-discovery-api.git

---
